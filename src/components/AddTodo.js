import React, {Component} from 'react';
import shortid from 'shortid'

export default class AddTodo extends Component {
    constructor(props) {
        super(props);
        this.state = {
            text: ''
        }
    }

    onChangeHandler = (event) => {
        this.setState({text: event.target.value})
    }

    onSubmitHandler = () => {
        this.props.onSubmit({
            id: shortid.generate(),
            text: this.state.text,
            complete: false
        });
        this.setState({text: ''})
    };

    render() {
        return <div>
                <input
                    value={this.state.text}
                    onChange={this.onChangeHandler}
                    placeholder='todo...'
                />
                <button onClick={this.onSubmitHandler}>Add Todo</button>
        </div>
    }
}
