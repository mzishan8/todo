import React, {Component} from 'react';
import AddTodo from "./AddTodo";
import Todo from "./Todo";

export default class TodoList extends Component {
    constructor(props) {
        super(props);
        this.state = {
            todos: []
        }
    }

    addTodo = (todo) => {
        this.setState({todos: [todo, ...this.state.todos]});
    };

    toggleComplete(id) {
        this.setState({
            todos: this.state.todos.map(todo => {
                if (todo.id === id) {
                    return {
                        ...todo,
                        complete: !todo.complete
                    };
                }
                return todo;
            })
        });
    }
    delete = (id)=>{
        this.setState({
            todos: this.state.todos.filter(todo => {
                if (todo.id === id) {
                    return false;
                }
                return true;
            })
        });
    }
    render() {
        return <div>
            <AddTodo onSubmit={this.addTodo}/>
            <div>
                {this.state.todos.map(todo =>
                    <Todo
                        key={todo.id}
                        toggleComplete={() => this.toggleComplete(todo.id)}
                        todo={todo}
                        delete={() => this.delete(todo.id)}
                    />
                )}
                <div>Todo Left: {this.state.todos.filter(todo => !todo.complete).length}</div>
            </div>
        </div>
    }
}
