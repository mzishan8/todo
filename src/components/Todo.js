import React from 'react';

export default function Todo(props) {
    return <div>
        <div
            style={{
                textDecoration: props.todo.complete ? 'line-through' : ""
            }}
            onClick={props.toggleComplete}
        >
            {props.todo.text}
        </div>
        <button onClick={props.delete}>X</button>
    </div>
}